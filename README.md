README

This app is going to be a mix of Raleigh trivia, maybe some observations as a newcomer to the area, ect. Eventually I would like to have the facts randomized in an array so that the user types their name and based on something (like the first letter) they get a randomized raleigh fact. 

I am eventually going to add oAuth so that only I can edit the facts, and so that users won't see the facts/index page with options to edit, add, delete. The welcome page will instead display the outside password and then allow them to input their name. 

This project was inspired by my newly adopted town of residence, Raleigh NC. 
